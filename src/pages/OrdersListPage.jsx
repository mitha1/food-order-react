import { useEffect, useState } from "react";
import { collection, getDocs, where } from "firebase/firestore";
import { firestore } from "../firebase";
import { formatCurrency } from "../utils/helpers";
import { useSelector } from "react-redux";
import { getUser } from "../redux/features/authSlice";

function OrdersListPage() {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = useSelector(getUser);

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        if (!user || !user.email) {
          throw new Error("User email not available.");
        }
        const querySnapshot = await getDocs(
          collection(firestore, "orders"),
          where("email", "==", user.email)
        );
        const ordersData = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setOrders(ordersData);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching orders: ", error);
        setLoading(false);
      }
    };

    if (user) {
      fetchOrders();
    }
  }, [user]); // Memperbarui saat user berubah

  if (!user) {
    return (
      <div className="flex h-full items-center justify-center mt-10">
        <div className="text-center">
          <p className="text-lg font-semibold mb-2">
            Please login with your email to see the Order Status!
          </p>
        </div>
      </div>
    );
  }

  if (loading) {
    return (
      <div className="flex h-full items-center justify-center mt-10">
        <div className="text-center">
          <p className="text-lg font-semibold mb-2">
            Loading...
          </p>
        </div>
      </div>
    );
  }

  return (
    <div className="min-h-screen p-4 bg-gray-100">
      <h1 className="text-2xl font-bold mb-4">Orders List</h1>
      <div className="overflow-x-auto bg-white shadow-md rounded-lg">
        <table className="min-w-full bg-white">
          <thead>
            <tr>
              <th className="py-2 px-4 border-b">Name</th>
              <th className="py-2 px-4 border-b">Phone</th>
              <th className="py-2 px-4 border-b">Order Type</th>
              <th className="py-2 px-4 border-b">Status</th>
              <th className="py-2 px-4 border-b">Total Price</th>
              <th className="py-2 px-4 border-b">Created At</th>
              <th className="py-2 px-4 border-b">Items</th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order) => (
              <tr key={order.id}>
                <td className="py-2 px-4 border-b">{order.name}</td>
                <td className="py-2 px-4 border-b">{order.phone}</td>
                <td className="py-2 px-4 border-b">{order.orderType}</td>
                <td className="py-2 px-4 border-b">{order.status}</td>
                <td className="py-2 px-4 border-b">{formatCurrency(order.totalPrice)}</td>
                <td className="py-2 px-4 border-b">{new Date(order.createdAt).toLocaleString()}</td>
                <td className="py-2 px-4 border-b">
                  {order.items.map((item, index) => (
                    <div key={index}>
                      {item.qty} x {item.title}
                    </div>
                  ))}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default OrdersListPage;

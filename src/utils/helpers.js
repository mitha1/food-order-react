export const formatCurrency = (value) =>
  new Intl.NumberFormat("id", { style: "currency", currency: "IDR" }).format(
    value,
  );

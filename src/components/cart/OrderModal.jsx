import React from "react";
import { MdClose } from "react-icons/md";
import Button from "../Button";

const OrderModal = ({ showModal, setShowModal, onSelectOption }) => {
  if (!showModal) return null;

  return (
    <>
      <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-50">
        <div className="bg-white rounded-lg shadow-lg w-[300px] p-6">
          <div className="flex justify-between items-center mb-4">
            <h3 className="text-lg font-bold">Choose Order Type</h3>
            <MdClose
              className="cursor-pointer text-xl"
              onClick={() => setShowModal(false)}
            />
          </div>
          <div className="flex flex-col gap-4">
            <Button styles="w-full" onClick={() => onSelectOption("Delivery")}>
              Delivery
            </Button>
            <Button styles="w-full" onClick={() => onSelectOption("Pickup")}>
              Pickup
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderModal;
